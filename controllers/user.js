var User = require('../models/user');

//Metodo que almacena en DB
var add = (req, res, next)=>{
    var user = new User({name: req.body.name, age: req.body.age});
    req.result=user;
    next();
};

var isMayor = (req, res, next)=>{
   if(req.body.edad>=18) req.isMayor=true
   else req.isMayor=false
    next();
};

//Metodo que devuelve un valor a la vista
var view =  (req, res)=>{
    if(req.result)
      res.sendFile(path.resolve('views/index.html'),{req, isMayor});
    else
      res.sendFile(path.resolve('views/error.html'));
};

module.exports={
add,
isMayor,
view
}