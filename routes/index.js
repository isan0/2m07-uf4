var express = require('express');
var app = express();

//Definimos la ruta donde encontrar los controladores usando la libreria Path
var path = require("path");
var ctrlDir = path.resolve("controllers/");


//Importamos el controllador
var userCtrl = require(path.join(ctrlDir, "user"));
var router = express.Router();

//Link routes and functions

app.post('/isMayor', userCtrl.add);
app.post('/isMayor', userCtrl.isMayor);
app.post('/isMayor', userCtrl.view);

module.exports = router;